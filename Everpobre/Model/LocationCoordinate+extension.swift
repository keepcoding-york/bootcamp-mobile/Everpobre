//
//  LocationCoordinate+extension.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 16/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import Foundation

extension LocationCoordinate {
    enum key: String {
        case entityName = "LocationCoordinate"
    }
}
