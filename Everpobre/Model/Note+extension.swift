//
//  Note+extension.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 08/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import Foundation

extension Note {
    enum key: String {
        case entityName = "Note"
        case title
        case notebook
        case notebookName = "notebook.name"
        case notebookDefault = "notebook.defaultNotebook"
        case createDate
        case modificationDate
    }
}
