//
//  Tag+extension.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 11/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import Foundation

extension Tag {
    enum key: String {
        case entityName = "Tag"
        case name
    }
}
