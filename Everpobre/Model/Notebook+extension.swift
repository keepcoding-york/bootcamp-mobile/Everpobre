//
//  Notebook+extension.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 05/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import Foundation

extension Notebook {
    enum key: String {
        case entityName = "Notebook"
        case name
        case notes
        case defaultNotebook
    }
}
