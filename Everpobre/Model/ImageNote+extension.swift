//
//  ImageNote.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 09/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import Foundation

extension ImageNote {
    enum key: String {
        case entityName = "ImageNote"
    }
}
