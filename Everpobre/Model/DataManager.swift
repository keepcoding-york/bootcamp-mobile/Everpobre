//
//  DataManager.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 12/03/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit
import CoreData

struct CoreDataManager {
    
    static let shared = CoreDataManager()
    
    let persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Everpobre")
        container.loadPersistentStores { (storeDescription, err) in
            if let err = err {
                fatalError("Loading of store failed: \(err)")
            }
//            container.viewContext.automaticallyMergesChangesFromParent = true
        }
        return container
    }()
    
    func fetchNotebooks() -> [Notebook] {
        let context = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<Notebook>(entityName: Notebook.key.entityName.rawValue)
        do {
            let notebooks = try context.fetch(fetchRequest)
            return notebooks
        } catch let fetchErr {
            print("Failed to fetch notebooks:", fetchErr)
            return []
        }
    }
    
    func fetchTags() -> [Tag] {
        let context = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<Tag>(entityName: Tag.key.entityName.rawValue)
        do {
            let tags = try context.fetch(fetchRequest)
            return tags
        } catch let fetchErr {
            print("Failed to fetch notebooks:", fetchErr)
            return []
        }
    }
    
    
    func createNotebook(name: String, notes: [Note]?, isDefault: Bool = false) -> (Notebook?, Error?) {
        let context = persistentContainer.viewContext
        
        let notebook = NSEntityDescription.insertNewObject(forEntityName: Notebook.key.entityName.rawValue, into: context) as! Notebook
        
        notebook.name = name
        notebook.defaultNotebook = isDefault
        
        if let notesOfNotebook = notes {
            notebook.notes = NSSet(array: notesOfNotebook)
        }
        
        do {
            try context.save()
            return (notebook, nil)
        } catch let err {
            print("Failed to create notebook:", err)
            return (nil, err)
        }
    }
    
    func createNote(title: String, notebook: Notebook?) -> (Note?, Error?) {
        let context = persistentContainer.viewContext
        
        let note = NSEntityDescription.insertNewObject(forEntityName: Note.key.entityName.rawValue, into: context) as! Note
        
        note.title = title
        note.createDate = Date()
        note.modificationDate = Date()

        
        if let notebookOfNote = notebook {
            note.notebook = notebookOfNote
        } else {
            let request: NSFetchRequest<Notebook> = Notebook.fetchRequest()
            request.predicate = NSPredicate(format: "defaultNotebook == %@", NSNumber(value: true))
            
            let notebookDefaultCoreData = try? context.fetch(request)
            
            note.notebook = notebookDefaultCoreData?.first
        }
        
        do {
            try context.save()
            return (note, nil)
        } catch let err {
            print("Failed to create notebook:", err)
            return (nil, err)
        }
    }
    
    func createTag(name: String, note: Note?) -> (Tag?, Error?) {
        let context = persistentContainer.viewContext
        
        let tag = NSEntityDescription.insertNewObject(forEntityName: Tag.key.entityName.rawValue, into: context) as! Tag
        
        tag.name = name
        
        if let noteOfTag = note {
            tag.note?.adding(noteOfTag)
        }
        
        do {
            try context.save()
            return (tag, nil)
        } catch let err {
            print("Failed to create notebook:", err)
            return (nil, err)
        }
    }
}









