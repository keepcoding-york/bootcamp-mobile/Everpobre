//
//  ImageAnimatedViewController.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 21/03/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

protocol ImageAnimatedViewControllerDelegate: class {
    func didChangesPositionImage()
}

class ImageAnimatedViewController: UIImageView {
    
    var imageNote: ImageNote!
    
    weak var delegate: ImageAnimatedViewControllerDelegate?
    
    var leftConstraint: NSLayoutConstraint!
    var topConstraint: NSLayoutConstraint!
    
    var widthConstraint: NSLayoutConstraint!
    var heightConstraint: NSLayoutConstraint!
    
    let SCALE_MAX: CGFloat = 1
    let SCALE_MIN: CGFloat = 0.5
    
    var isVisibleButtonRemove = false
    
    
    var tintOverImage: UIView = {
        let tintView = UIView()
        tintView.backgroundColor = UIColor(white: 1, alpha: 0.5)
        tintView.translatesAutoresizingMaskIntoConstraints = false
        return tintView
    }()
    
    var removeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "image-remove"), for: .normal)
        button.tintColor = .black
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleRemoveImage), for: .touchUpInside)
        return button
    }()
    
    var relativePoint: CGPoint!
    
    // MARK: - Init
    override init(image: UIImage?) {
        super.init(image: image)
    }
    
    convenience init(imageNote: ImageNote) {
        self.init(image: UIImage(data: imageNote.image!))
        self.imageNote = imageNote
        transform = CGAffineTransform(rotationAngle: CGFloat(imageNote.rotation))
        setupUI()
        setupGestures()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    func setupUI() {
        isUserInteractionEnabled = true
        contentMode = .scaleAspectFit
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setupGestures() {
        let longPress =  UILongPressGestureRecognizer(target: self, action: #selector(handleMove))
        longPress.minimumPressDuration = 0.1
        addGestureRecognizer(longPress)
        
        let pinchGesture =  UIPinchGestureRecognizer(target: self, action: #selector(handleScale))
        addGestureRecognizer(pinchGesture)
        
        let doubleTapGesture = UITapGestureRecognizer(target: self, action:#selector(handleDoubleTap))
        doubleTapGesture.numberOfTapsRequired = 2
        addGestureRecognizer(doubleTapGesture)
        
        let rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(handleRotate))
        addGestureRecognizer(rotateGesture)
    }
    
    func setupSize() {
        guard let imageWrapped = image, let superview = superview else { return }
        let scale = CGFloat((imageNote?.scale)!)
        let aspectRatio: CGFloat
        if imageWrapped.size.height < imageWrapped.size.width {
            aspectRatio = imageWrapped.size.height / imageWrapped.size.width
            widthConstraint = widthAnchor.constraint(equalTo: superview.widthAnchor, multiplier: scale)
            heightConstraint = heightAnchor.constraint(equalTo: superview.widthAnchor, multiplier: aspectRatio * scale)
        } else {
            aspectRatio = imageWrapped.size.width / imageWrapped.size.height
            heightConstraint = heightAnchor.constraint(equalTo: superview.heightAnchor, multiplier: scale)
            widthConstraint = widthAnchor.constraint(equalTo: superview.heightAnchor, multiplier: aspectRatio * scale)
        }
        widthConstraint.isActive = true
        heightConstraint.isActive = true
    }
    
    func setupPosition() {
        guard let superview = superview else { return }
        
        if let viewInheritScrollView = superview as? UIScrollView {
            topConstraint = topAnchor.constraint(equalTo: viewInheritScrollView.contentLayoutGuide.topAnchor, constant: CGFloat(imageNote!.y))
            leftConstraint = leftAnchor.constraint(equalTo: viewInheritScrollView.contentLayoutGuide.leftAnchor, constant: CGFloat(imageNote!.x))
        }
        else {
            topConstraint = topAnchor.constraint(equalTo: superview.topAnchor, constant: CGFloat(imageNote!.y))
            leftConstraint = leftAnchor.constraint(equalTo: superview.leftAnchor, constant: CGFloat(imageNote!.x))
        }
        topConstraint.isActive =  true
        leftConstraint.isActive = true
    }
    
    func setupConstraint() {
        setupSize()
        setupPosition()
    }
    
    func setupTransform() {
        
    }
    
    // MARK: - Handler
    @objc func handleRemoveImage(){
        let context = CoreDataManager.shared.persistentContainer.viewContext
        context.delete(imageNote!)
        
        do {
            try context.save()
            self.removeFromSuperview()
        } catch let saveErr {
            print("Failed to delete image:", saveErr)
        }
    }
    
    //MARK: Handler Gesture
    @objc func handleMove(recognizer: UITapGestureRecognizer){
        if !isVisibleButtonRemove {
            switch recognizer.state {
            case .began:
                relativePoint = recognizer.location(in: recognizer.view)
                addTintOverImage()
            case .changed:
                let location = recognizer.location(in: superview)
                if CheckOverflowX(coordinateX: location.x - relativePoint.x) {
                    leftConstraint.constant = location.x - relativePoint.x
                    imageNote?.x = Float(location.x - relativePoint.x)
                }
                if CheckOverflowY(coordinateY: location.y - relativePoint.y){
                    topConstraint.constant = location.y - relativePoint.y
                    imageNote?.y = Float(location.y - relativePoint.y)
                }
            case .ended, .cancelled:
                removeTintOverImage()
            default:
                break
            }
            delegate?.didChangesPositionImage()
        }
    }
    
    @objc func handleScale(recognizer: UIPinchGestureRecognizer){
        if !isVisibleButtonRemove {
            switch recognizer.state {
            case .began:
                addTintOverImage()
            case .changed:
                let pinchScale = recognizer.scale
                let scaleCurrent = CGFloat((imageNote?.scale)!)
                let scale: CGFloat
                if (pinchScale >= 1) {
                    scale = (scaleCurrent + (pinchScale - 1))
                    if (scale < SCALE_MAX) {
                        if(checkOverflowWidth(scale: scale)){
                            //                        scaleCurrent = scale
                            imageNote?.scale = Float(scale)
                            NSLayoutConstraint.deactivate([widthConstraint, heightConstraint])
                            setupSize()
                        }
                    }
                } else {
                    scale = (scaleCurrent - (1 - pinchScale))
                    if (scale > SCALE_MIN) {
                        //                    scaleCurrent = scale
                        imageNote?.scale = Float(scale)
                        NSLayoutConstraint.deactivate([widthConstraint, heightConstraint])
                        setupSize()
                    }
                }
            case .ended, .cancelled:
                removeTintOverImage()
            default:
                break
            }
            delegate?.didChangesPositionImage()
        }
    }
    
    @objc func handleDoubleTap(recognizer: UITapGestureRecognizer) {
        if recognizer.state == .ended{
            if isVisibleButtonRemove {
                removeTintOverImage()
                removeButtonRemove()
            } else {
                addTintOverImage()
                addButtonRemove()
            }
            isVisibleButtonRemove = !isVisibleButtonRemove
        }
    }
    
    @objc func handleRotate(recognizer: UIRotationGestureRecognizer) {
        if !isVisibleButtonRemove {
            switch recognizer.state {
            case .began:
                addTintOverImage()
            case .changed:
                let rotation = recognizer.rotation
                transform = CGAffineTransform(rotationAngle: rotation)
                imageNote?.rotation = Float(rotation)
            case .ended, .cancelled:
                removeTintOverImage()
            default:
                break
            }
            delegate?.didChangesPositionImage()
        }
    }
    
    // MARK: - Check Overflow
    func checkOverflowWidth(scale: CGFloat) -> Bool {
        let newSizeWidth = superview!.frame.width * scale
        let coordinateSideRight = newSizeWidth + leftConstraint.constant
        let maxCoordinateRight = (superview?.frame.width)!
        return coordinateSideRight < maxCoordinateRight
    }
    
    func CheckOverflowX(coordinateX: CGFloat) -> Bool {
        return (coordinateX > 0) && ((coordinateX + frame.width) < (superview?.frame.width)!)
    }
    
    func CheckOverflowY(coordinateY: CGFloat) -> Bool {
        return coordinateY > 0
    }
    
    // MARK: - Helpers
    
    func addButtonRemove() {
        addSubview(removeButton)
        removeButton.widthAnchor.constraint(equalToConstant: 45).isActive = true
        removeButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
        removeButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        removeButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    func removeButtonRemove() {
        removeButton.removeFromSuperview()
    }
    
    func addTintOverImage() {
        addSubview(tintOverImage)
        tintOverImage.fillSuperview()
    }
    
    func removeTintOverImage() {
        tintOverImage.removeFromSuperview()
    }
}
