//
//  UIView.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 21/03/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

extension UIView {
    
    func fillSuperview() {
        anchor(top: superview?.topAnchor, leading: superview?.leadingAnchor, bottom: superview?.bottomAnchor, trailing: superview?.trailingAnchor)
    }
    
    func anchorSize(to view: UIView) {
        widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }
    
    func anchor(top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, trailing: NSLayoutXAxisAnchor?, padding: UIEdgeInsets = .zero, size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom).isActive = true
        }
        
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: -padding.right).isActive = true
        }
        
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
    }
}

// MARK: - TODO Not Working
extension UIView {
    
    enum UIViewBorderSide {
        case Left, Right, Top, Bottom
    }
    
    func addBorder(toSide side: UIViewBorderSide, withColor color: UIColor, andThickness thickness: CGFloat) {
        
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
        }
        
        layer.addSublayer(border)
    }
}

public

// MARK: - TODO Not Working
extension UIButton {
    enum UIButtonBorderSide {
        case Top, Bottom, Left, Right
    }
    
    public func addBorderToButton(toSide side: UIButtonBorderSide, withColor color: UIColor, andThickness thickness: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        switch side {
        case .Top:
            border.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: thickness)
        case .Bottom:
            border.frame = CGRect(x: 0, y: self.frame.size.height - thickness, width: self.frame.size.width, height: thickness)
        case .Left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.size.height)
        case .Right:
            border.frame = CGRect(x: self.frame.size.width - thickness, y: 0, width: thickness, height: self.frame.size.height)
        }
        
        self.layer.addSublayer(border)
    }
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

//extension UIView {
//    
//    var safeTopAnchor: NSLayoutYAxisAnchor {
//        if #available(iOS 11.0, *) {
//            return self.safeAreaLayoutGuide.topAnchor
//        } else {
//            return self.topAnchor
//        }
//    }
//    
//    var safeLeftAnchor: NSLayoutXAxisAnchor {
//        if #available(iOS 11.0, *){
//            return self.safeAreaLayoutGuide.leftAnchor
//        }else {
//            return self.leftAnchor
//        }
//    }
//    
//    var safeRightAnchor: NSLayoutXAxisAnchor {
//        if #available(iOS 11.0, *){
//            return self.safeAreaLayoutGuide.rightAnchor
//        }else {
//            return self.rightAnchor
//        }
//    }
//    
//    var safeBottomAnchor: NSLayoutYAxisAnchor {
//        if #available(iOS 11.0, *) {
//            return self.safeAreaLayoutGuide.bottomAnchor
//        } else {
//            return self.bottomAnchor
//        }
//    }
//}
