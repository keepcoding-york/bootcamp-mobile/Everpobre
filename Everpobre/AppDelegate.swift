//
//  AppDelegate.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 08/03/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    public var notebookDefault: Notebook?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        setupUI()
        notebookDefault = getDefaultNotebook()
        
        window = UIWindow()
        window?.rootViewController = getRootViewController()
        window?.makeKeyAndVisible()
        
        return true
    }
}

extension AppDelegate {
    func setupUI() {
//        UINavigationBar.appearance().prefersLargeTitles = true
        UINavigationBar.appearance().tintColor = .darkGray
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.darkGray]
    }
    
    func getDefaultNotebook() -> Notebook? {
        let request: NSFetchRequest<Notebook> = Notebook.fetchRequest()
        request.predicate = NSPredicate(format: "\(Notebook.key.defaultNotebook.rawValue) == %@", NSNumber(value: true))
        
        let context = CoreDataManager.shared.persistentContainer.viewContext
        
        let notebookDefaultCoreData = try? context.fetch(request)
        
        if let notebook = notebookDefaultCoreData?.first {
            return notebook
        }
        
        let tuple = CoreDataManager.shared.createNotebook(name: "Notebook Default", notes: nil, isDefault: true)
        guard let notebook = tuple.0 else { return nil }
        return notebook
    }
    
    func getRootViewController() -> UIViewController {
        let notebooksController = NotebooksController()
        let splitViewController = UISplitViewController()
        splitViewController.preferredDisplayMode = .allVisible
        splitViewController.delegate = self
        
        if let notes = notebookDefault?.notes?.sortedArray(using: [NSSortDescriptor(key: Note.key.modificationDate.rawValue, ascending: false)]) {
            if notes.count > 0 {
                let note = notes[0] as? Note
                let noteController = NoteViewController()
                noteController.note = note
                noteController.delegate = notebooksController
                let navigationController = UINavigationController(rootViewController: noteController)
                noteController.navigationItem.leftItemsSupplementBackButton = true
                noteController.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
                splitViewController.viewControllers = [UINavigationController(rootViewController: notebooksController), navigationController]
            } else {
                splitViewController.viewControllers = [UINavigationController(rootViewController: notebooksController), NoNoteSelectedViewController()]
            }
        }
        return splitViewController
    }
}

// MARK: - UISplitViewControllerDelegate
extension AppDelegate: UISplitViewControllerDelegate {
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
        return true
    }
}
