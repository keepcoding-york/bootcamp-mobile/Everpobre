#  Personal

## Image como string

let image = info[UIImagePickerControllerOriginalImage] as! UIImage
//create and NSTextAttachment and add your image to it.
let attachment = NSTextAttachment()
attachment.image = image
//calculate new size.  (-20 because I want to have a litle space on the right of picture)
let newImageWidth = (bodyTextView.bounds.size.width)
let scale = newImageWidth/image.size.width
let newImageHeight = image.size.height * scale
//resize this
attachment.bounds = CGRect.init(x: 0, y: 0, width: newImageWidth, height: newImageHeight)
//put your NSTextAttachment into and attributedString
let attributedString = NSAttributedString(attachment: attachment)
//add this attributed string to the current position.
bodyTextView.textStorage.insert(attributedString, at: bodyTextView.selectedRange.location)
picker.dismiss(animated: true, completion: nil)

## Reactive UI

https://medium.com/@augusteo/reactive-way-to-handle-dismissing-modal-view-controller-81af09f886f6
https://medium.com/@raulriera/custom-view-controller-presentations-the-right-way-53e8e8e8118b


## UI Layout
https://github.com/mamaral/Neon


## Multiple Selection
https://medium.com/ios-os-x-development/ios-multiple-selections-in-table-view-88dc2249c3a2

## Autolayout
Quick summary of the concepts:

- Intrinsic Content Size - Pretty self-explanatory, but views with variable content are aware of how big their content is and describe their content's size through this property. Some obvious examples of views that have intrinsic content sizes are UIImageViews, UILabels, UIButtons.

- Content Hugging Priority - The higher this priority is, the more a view resists growing larger than its intrinsic content size.

- Content Compression Resistance Priority - The higher this priority is, the more a view resists shrinking smaller than its intrinsic content size.

> Hugging => content does not want to grow
> Compression Resistance => content does not want to shrink

and an example:

Say you've got button like this:

`[       Click Me      ]`

and you've pinned the edges to a larger superview with priority 500.

Then, if Hugging priority > 500 it'll look like this:

`[Click Me]`

If Hugging priority < 500 it'll look like this:

`[       Click Me      ]`

If superview now shrinks then, if the Compression Resistance priority > 500, it'll look like this

`[Click Me]`

Else if Compression Resistance priority < 500, it could look like this:

`[Cli..]`

If it doesn't work like this then you've probably got some other constraints going on that are messing up your good work!

E.g. you could have it pinned to the superview with priority 1000. Or you could have a width priority. If so, this can be helpful:

Editor > Size to Fit Content


## Gesture Image

### Pinch

https://medium.com/@jeremysh/instagram-pinch-to-zoom-pan-gesture-tutorial-772681660dfe
https://codebass.blogspot.com.es/2017/10/swift-image-zooming-using-pinch.html
