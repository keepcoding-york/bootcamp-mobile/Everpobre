//
//  NoteViewController.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 19/03/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit
import CoreData

protocol NoteViewControllerDelegate: class {
    func didSaveNote()
}

class NoteViewController: UIViewController {
    
    weak var delegate: NoteViewControllerDelegate?
    let datePicker = UIDatePicker()
    var relativePoint: CGPoint!
    
    var note: Note? {
        didSet{
            titleNoteTextField.text = note?.title
            notebookButton.setTitle(note?.notebook?.name, for: .normal)
           
            if let dueDate = note?.dueDate {
                dueDateOfNoteButton.setTitle(dueDate.toString(dateFormatType: .date), for: .normal)
                dueDateOfNoteButton.tintColor = UIColor.black
            }
            
            if let tags = note?.tags {
                if tags.count != 0 {
                    tagsButton.setTitle("\(tags.count)", for: .normal)
                    tagsButton.tintColor = .black
                }
            }
            
            if  note?.location != nil {
                locationOfTheNoteButton.tintColor = .black
            }
            
            bodyTextView.text = note?.content
            
            note?.images?.forEach({ (imageEntity) in
                let image = imageEntity as! ImageNote
                let imageView = ImageAnimatedViewController(imageNote: image)
                bodyTextView.addSubview(imageView)
                imageView.setupConstraint()
                imageView.delegate = self
                imagesViewsOfNote?.append(imageView)
            })
            view.setNeedsLayout()
        }
    }

    
    var imagesViewsOfNote: [ImageAnimatedViewController]? = []
    
    let notebookButton: UIButton = {
        let button =  UIButton(type: .system)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        button.setTitleColor(.darkGray, for: .normal)
        button.setImage(#imageLiteral(resourceName: "notebook-edit"), for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 0)
        button.tintColor = UIColor.darkGray
        button.contentHorizontalAlignment = .left
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setContentHuggingPriority(.defaultLow, for: .horizontal)
        button.addTarget(self, action: #selector(handleNotebook), for: .touchUpInside)
        
        return button
    }()
    
    let titleNoteTextField: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.systemFont(ofSize: 24, weight: .black)
        textField.textColor = .darkGray
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.setContentHuggingPriority(.defaultLow, for: .horizontal)
        textField.setContentCompressionResistancePriority(.required, for: .horizontal)
        
        return textField
    }()
    
    let dueDateOfNoteButton: UIButton = {
        let button =  UIButton(type: .system)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        button.setImage(#imageLiteral(resourceName: "signs-edit"), for: .normal)
        button.tintColor = UIColor.lightGray
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setContentHuggingPriority(.required, for: .horizontal)
        button.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        button.addTarget(self, action: #selector(handleDate), for: .touchUpInside)
        
        return button
    }()
    
    let locationOfTheNoteButton: UIButton = {
        let button =  UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "location"), for: .normal)
        button.tintColor = UIColor.lightGray
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setContentHuggingPriority(.required, for: .horizontal)
        button.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 248), for: .horizontal)
        button.addTarget(self, action: #selector(handleMap), for: .touchUpInside)
        
        return button
    }()
    
    let tagsButton: UIButton = {
        let button =  UIButton(type: .system)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        button.setImage(#imageLiteral(resourceName: "tag"), for: .normal)
        button.tintColor = UIColor.lightGray
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setContentHuggingPriority(.required, for: .horizontal)
        button.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        button.addTarget(self, action: #selector(handleTag), for: .touchUpInside)
        
        return button
    }()
    
    let bodyTextView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 14)
        textView.textColor = .gray
        textView.clipsToBounds = true
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        return textView
    }()
    
    var exclusionPaths:[UIBezierPath] = []
    
    // MARK: - Init
    init() {
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: UIViewController lifecycle
    override func loadView() {
        let view: UIView = {
            let view = UIView()
            view.backgroundColor = .white
            return view
        }()
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(handleSave))
        navigationItem.title = "Note"
        
        setupLayout()
        setupGesture()
    }
    
    override func viewDidLayoutSubviews() {
        bodyTextView.textContainer.exclusionPaths = []
        
        for subview in bodyTextView.subviews {
            if let imageView = subview as? UIImageView {
                
                var rect = CGRect(x: imageView.frame.origin.x, y: imageView.frame.origin.y, width: imageView.frame.width, height: imageView.frame.height)
                rect = rect.insetBy(dx: -10, dy: -10)
                
                let paths = UIBezierPath(rect: rect)
                bodyTextView.textContainer.exclusionPaths.append(paths)
            }
        }
    }
    
    //MARK: - Setup
    func setupLayout() {
        
        let toolBarStackView = UIStackView(arrangedSubviews: [notebookButton, dueDateOfNoteButton, tagsButton, locationOfTheNoteButton])
        toolBarStackView.spacing = 10
        toolBarStackView.translatesAutoresizingMaskIntoConstraints = false
        toolBarStackView.distribution = .fill
        
        let noteStackView = UIStackView(arrangedSubviews: [titleNoteTextField, toolBarStackView, bodyTextView])
        noteStackView.translatesAutoresizingMaskIntoConstraints = false
        noteStackView.axis = .vertical
        noteStackView.spacing = 10
        noteStackView.distribution = .fill
        
        view.addSubview(noteStackView)
        
        NSLayoutConstraint.activate([
            noteStackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            noteStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            noteStackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            noteStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0)
            ])
    }
    
    func setupGesture() {
        let closeKeyboradGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleCloseKeyboard))
        closeKeyboradGesture.direction = .down
        view.addGestureRecognizer(closeKeyboradGesture)
        
        let addPhotoGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleCatchPhoto))
        bodyTextView.addGestureRecognizer(addPhotoGesture)
    }
    
    //MARK: - Handlers
    @objc func handleSave() {
        let context = CoreDataManager.shared.persistentContainer.viewContext
        
        note?.title = titleNoteTextField.text
        note?.modificationDate = Date()
        note?.content = bodyTextView.text
        
        try? context.save()
        
        delegate?.didSaveNote()
    }
    
    @objc func handleCloseKeyboard() {
        if bodyTextView.isFirstResponder {
            bodyTextView.resignFirstResponder()
        } else if titleNoteTextField.isFirstResponder {
            titleNoteTextField.resignFirstResponder()
        }
    }
    
    @objc func handleCatchPhoto(longPressGesture: UILongPressGestureRecognizer) {
        switch longPressGesture.state {
        case .began:
            relativePoint = longPressGesture.location(in: longPressGesture.view)
            let actionSheetAlert = UIAlertController(title: NSLocalizedString("Add photo", comment: "Add photo"), message: nil, preferredStyle: .actionSheet)
            
            if let popoverPresentationController = actionSheetAlert.popoverPresentationController {
                popoverPresentationController.sourceView = self.view
                popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverPresentationController.permittedArrowDirections = []
            }
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            
            let useCamera = UIAlertAction(title: "Camara", style: .default) {
                (alertAction) in
                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
            }
            
            let usePhotoLibrary = UIAlertAction(title: "Photo library", style: .default) {
                (alertAction) in
                imagePicker.sourceType = .photoLibrary
                self.present(imagePicker, animated: true, completion: nil)
            }
            
            let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .destructive, handler: nil)
            
            
            actionSheetAlert.addAction(useCamera)
            actionSheetAlert.addAction(usePhotoLibrary)
            actionSheetAlert.addAction(cancel)
            
            present(actionSheetAlert, animated: true, completion: nil)
        default:
            break
        }
    }
    
    @objc func handleNotebook() {
        let selectNotebookController = SelectNotebookController(style: .plain)
        selectNotebookController.delegate = self
        if let notebookName = note?.notebook?.name {
         selectNotebookController.notebooksToHide = [notebookName]
        }
        navigationController?.pushViewController(selectNotebookController, animated: true)
    }
    
    @objc func handleMap() {
        let actionSheetAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if let popoverPresentationController = actionSheetAlert.popoverPresentationController {
            popoverPresentationController.sourceView = locationOfTheNoteButton
        }
        
        let setLocation = UIAlertAction(title: "Set location", style: .default) {
            (alertAction) in
            let mapController: MapController
            if let location = self.note?.location {
                mapController = MapController()
                mapController.locationCoordinate = location
            } else {
                let context = CoreDataManager.shared.persistentContainer.viewContext
                let locationCoordinate = NSEntityDescription.insertNewObject(forEntityName: LocationCoordinate.key.entityName.rawValue, into: context) as! LocationCoordinate
                
                self.note?.location = locationCoordinate
                mapController = MapController()
                mapController.locationCoordinate = locationCoordinate
                mapController.getLocation()
            }
            
            mapController.delegate = self
            self.navigationController?.pushViewController(mapController, animated: true)
        }
        setLocation.setValue(UIColor.black, forKeyPath: "titleTextColor")
        actionSheetAlert.addAction(setLocation)
        
        if note?.location != nil {
            let removeLocation = UIAlertAction(title: "Remove date", style: .destructive) {
                (alertAction) in
                self.note?.location = nil
                self.locationOfTheNoteButton.tintColor = .lightGray
            }
            actionSheetAlert.addAction(removeLocation)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheetAlert.addAction(cancel)
        
        present(actionSheetAlert, animated: true, completion: nil)
    }
    
    @objc func handleDate() {
        let actionSheetAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if let popoverPresentationController = actionSheetAlert.popoverPresentationController {
            popoverPresentationController.sourceView = dueDateOfNoteButton
        }
        
        let setDate = UIAlertAction(title: "Set date", style: .default) {
            (alertAction) in
            let selectDateController: SelectDateController
            if let date = self.note?.dueDate {
                selectDateController = SelectDateController(date: date)
            } else {
                selectDateController = SelectDateController()
            }
            
            selectDateController.delegate = self
            self.navigationController?.pushViewController(selectDateController, animated: true)
        }
        setDate.setValue(UIColor.black, forKeyPath: "titleTextColor")
        actionSheetAlert.addAction(setDate)
        
        if note?.dueDate != nil {
            let removeDate = UIAlertAction(title: "Remove date", style: .destructive) {
                (alertAction) in
                self.note?.dueDate = nil
                self.dueDateOfNoteButton.setTitle("", for: .normal)
                self.dueDateOfNoteButton.tintColor = .lightGray
            }
            actionSheetAlert.addAction(removeDate)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheetAlert.addAction(cancel)
        
        present(actionSheetAlert, animated: true, completion: nil)
    }
    
    @objc func handleTag() {
        let selectTagsController = SelectTagsController(note: note!)
        selectTagsController.delegate = self
        navigationController?.pushViewController(selectTagsController, animated: true)
    }
}

//MARK: - UINavigationControllerDelegate
extension NoteViewController: UINavigationControllerDelegate {
    
}

//MARK: - UIImagePickerControllerDelegate
extension NoteViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        let context = CoreDataManager.shared.persistentContainer.viewContext
        let imageEntity = NSEntityDescription.insertNewObject(forEntityName: ImageNote.key.entityName.rawValue, into: context) as! ImageNote
        imageEntity.x = 0
        imageEntity.y = Float(relativePoint.y)
        imageEntity.scale = 1
        imageEntity.rotation = 0
        imageEntity.image = UIImageJPEGRepresentation(image, 0.8)
        imageEntity.note = note
        note?.addToImages(imageEntity)
        
        
        let imageView = ImageAnimatedViewController(imageNote: imageEntity)
        bodyTextView.addSubview(imageView)
        imageView.setupConstraint()
        imageView.delegate = self
        imagesViewsOfNote?.append(imageView)
        
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK: - SelectNotebookControllerDelegate
extension NoteViewController: SelectNotebookControllerDelegate {
    func didSelectNotebook(notebook: Notebook) {
        note?.notebook = notebook
        notebookButton.setTitle(notebook.name, for: .normal)
        let context = CoreDataManager.shared.persistentContainer.viewContext
        try? context.save()
    }
}

//MARK: - ImageAnimatedViewControllerDelegate
extension NoteViewController: ImageAnimatedViewControllerDelegate {
    func didChangesPositionImage() {
        view.setNeedsLayout()
    }
}

//MARK: - SelectDateControllerDelegate
extension NoteViewController: SelectDateControllerDelegate {
    func didSelectDate(date: Date) {
        note?.dueDate = date
        dueDateOfNoteButton.setTitle(date.toString(dateFormatType: .date), for: .normal)
        dueDateOfNoteButton.tintColor = .black
    }
}

//MARK: - SelectTagsControllerDelegate
extension NoteViewController: SelectTagsControllerDelegate {
    func didSelectTags() {
        guard let tags = note?.tags else { return }
        if tags.count != 0 {
            tagsButton.setTitle("\(tags.count)", for: .normal)
            tagsButton.tintColor = .black
        } else {
            tagsButton.setTitle("", for: .normal)
            tagsButton.tintColor = .lightGray
        }
    }
}

//MARK: - MapControllerDelegate
extension NoteViewController: MapControllerDelegate {
    func didSaveLocation() {
        locationOfTheNoteButton.tintColor = .black
    }
}
