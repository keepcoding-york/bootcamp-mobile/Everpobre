//
//  NoteCell.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 11/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

class NoteCell: UITableViewCell {
    
    let dateStyleText = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 12), NSAttributedStringKey.foregroundColor : UIColor.gray]
    let contentStyleText = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 12), NSAttributedStringKey.foregroundColor : UIColor.lightGray]

    var note: Note? {
        didSet {
            textLabel?.text = "\(note?.title ?? "") - \(note?.notebook?.name ?? "")"
            let detailText = NSMutableAttributedString(string:"\(note?.modificationDate?.toString(dateFormatType: .dateWithTime) ?? "")", attributes: dateStyleText)
            
            if note?.content != nil && !(note?.content?.isEmpty)! {
                let contentText = NSMutableAttributedString(string:" | \(note?.content?.prefix(60) ?? "")", attributes: contentStyleText)
                detailText.append(contentText)
            }
            detailTextLabel?.attributedText = detailText
        }
    }
    
    // MARK: - Init
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        textLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        textLabel?.textColor = .darkGray
        detailTextLabel?.font =  UIFont.systemFont(ofSize: 12, weight: .regular)
        detailTextLabel?.textColor = .gray
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
