//
//  NotebookHeaderInSection.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 12/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit
import CoreData

class NotebookHeaderInSection: UIView {
    
    let notebookButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.lightGray, for: .normal)
        button.addBorderToButton(toSide: .Bottom, withColor: UIColor.black, andThickness: 20.0)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFont.systemFont(ofSize: 24, weight: .light)
        
        return button
    }()
    
    let notebookMenuButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "menu-three-dots"), for: .normal)
        button.tintColor = .black
        button.imageView?.contentMode = .scaleAspectFit
        button.translatesAutoresizingMaskIntoConstraints = false
        button.anchor(top: nil, leading: nil, bottom: nil, trailing: nil, size: CGSize(width: 20, height: 20))
        button.addTarget(self, action: #selector(handleMenu), for: .touchUpInside)
        
        return button
    }()

    
    var notebook: Notebook? {
        didSet {
            notebookButton.setTitle(notebook?.name, for: .normal)
        }
    }
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    func setupLayout() {
        let headerSectionStackView = UIStackView(arrangedSubviews: [notebookButton, notebookMenuButton])
        backgroundColor = .white
        addSubview(headerSectionStackView)
        headerSectionStackView.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: UIEdgeInsetsMake(0, 20, 0, 20))
        headerSectionStackView.spacing = 10
        headerSectionStackView.distribution = .fill
        headerSectionStackView.layoutMargins.top = 30
        headerSectionStackView.layoutMargins.bottom = 20
        headerSectionStackView.isLayoutMarginsRelativeArrangement = true
    }
    
    // MARK: - Handlers
    @objc func handleMenu() {
        print("handleMenu \(notebook?.name ?? "") is default \(notebook?.defaultNotebook ?? false) ?? false")
        let alert = UIAlertController(title: "\(notebook?.name ?? "")", message: nil, preferredStyle: .actionSheet)
        
        if let popoverPresentationController = alert.popoverPresentationController {
            popoverPresentationController.sourceView = notebookMenuButton
        }

        if !(notebook?.defaultNotebook)! {
        alert.addAction(UIAlertAction(title: "Set notebook default", style: .default, handler: {(_) in
            let context = CoreDataManager.shared.persistentContainer.viewContext

            let request: NSFetchRequest<Notebook> = Notebook.fetchRequest()
            request.predicate = NSPredicate(format: "defaultNotebook == %@", NSNumber(value: true))

            let notebookDefaultCoreData = try? context.fetch(request)
            notebookDefaultCoreData?.first?.defaultNotebook = false

            self.notebook?.defaultNotebook = true

            try? context.save()
        }))

            alert.addAction(UIAlertAction(title: "Remove", style: .default, handler: {(_) in
                if (self.notebook?.notes?.count)! > 0 {
                    let removeNotebookController = RemoveNotebookController()
                    removeNotebookController.notebook = self.notebook
                    let navigationController = UINavigationController(rootViewController: removeNotebookController)
                    self.parentViewController?.present(navigationController, animated: true, completion: nil)
                } else {
                    let context = CoreDataManager.shared.persistentContainer.viewContext
                    context.delete(self.notebook!)
                    try! context.save()
                }
            }))
        }

        alert.addAction(UIAlertAction(title: "Set name", style: .default, handler: {(_) in
            self.changeNameOfNotebook()
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.parentViewController?.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Functions
    func changeNameOfNotebook() {
        let alert = UIAlertController(title: "Change name of notebook", message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder =  "Enter name for notebook"
            textField.text = self.notebook?.name
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            if let nameNotebook = alert?.textFields![0].text {
                let context = CoreDataManager.shared.persistentContainer.viewContext
                self.notebook?.name = nameNotebook
                try? context.save()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        
        self.parentViewController?.present(alert, animated: true, completion: nil)
    }
}

