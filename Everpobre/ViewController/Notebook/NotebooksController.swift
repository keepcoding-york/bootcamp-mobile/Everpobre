//
//  NotebooksController.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 05/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit
import CoreData

class NotebooksController: UITableViewController {
    
    private let CELL_ID = "CELL_ID_NOTEBOOKSCONTROLLER"
    private let notesSortDescriptor = NSSortDescriptor(key: Note.key.modificationDate.rawValue, ascending: false)
    
    // MARK: - NSFetchedResultsController
    lazy var fetchedResultsController: NSFetchedResultsController<Notebook> = {
        
        let context = CoreDataManager.shared.persistentContainer.viewContext
        let request: NSFetchRequest<Notebook> = Notebook.fetchRequest()
        request.sortDescriptors = [
            NSSortDescriptor(key: Notebook.key.defaultNotebook.rawValue, ascending: false),
            NSSortDescriptor(key: Notebook.key.name.rawValue, ascending: true)
        ]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch let err {
            print(err)
        }
        
        return fetchedResultsController
    }()
    
    //MARK: UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - TableView DataSource
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let notebook = fetchedResultsController.object(at: IndexPath(row: section, section: 0))
        let headerView = NotebookHeaderInSection()
        headerView.notebook = notebook
        return headerView
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.fetchedObjects?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let notebook = fetchedResultsController.object(at: IndexPath(row: section, section: 0))
        return notebook.notes?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notebook = fetchedResultsController.object(at: IndexPath(row: indexPath.section, section: 0))
        let notes = notebook.notes?.sortedArray(using: [notesSortDescriptor]) as! [Note]
        let note = notes[indexPath.row]
        
        let noteCell = NoteCell(style: .subtitle, reuseIdentifier: CELL_ID)
        noteCell.note = note

        return noteCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notebook = fetchedResultsController.object(at: IndexPath(row: indexPath.section, section: 0))
        let notes = notebook.notes?.sortedArray(using: [notesSortDescriptor]) as! [Note]
        let note = notes[indexPath.row]
        
        let noteController = NoteViewController()
        noteController.note = note
        noteController.delegate = self
        
        noteController.navigationItem.leftItemsSupplementBackButton = true
        noteController.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        
        if (splitViewController?.isCollapsed)! {
            splitViewController?.showDetailViewController(noteController, sender: nil)
        } else {
            splitViewController?.showDetailViewController(UINavigationController(rootViewController: noteController), sender: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (_, indexPath) in
            let notebook = self.fetchedResultsController.object(at: IndexPath(row: indexPath.section, section: 0))
            let notes = notebook.notes?.sortedArray(using: [self.notesSortDescriptor]) as! [Note]
            let note = notes[indexPath.row]
            
            if let viewControllers = self.splitViewController?.viewControllers {
                if viewControllers.count > 1 {
                    if let detailNavigationController = viewControllers.last as? UINavigationController {
                        if let detailViewController = detailNavigationController.viewControllers.first as? NoteViewController {
                            if (detailViewController.note?.createDate == note.createDate)&&(detailViewController.note?.title == note.title) {
                                self.splitViewController?.showDetailViewController(NoNoteSelectedViewController(), sender: true)
                            }
                        }
                    }
                }
            }

            let context = CoreDataManager.shared.persistentContainer.viewContext
            context.delete(note)

            do {
                try context.save()
            } catch let saveErr {
                print("Failed to delete company:", saveErr)
            }
        }
        deleteAction.backgroundColor = .red

        return [deleteAction]
    }
    
    // MARK: - Setup UI
    func setupUI() {
        navigationItem.title = "Notebooks"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.register(NoteCell.self, forCellReuseIdentifier: CELL_ID)
        
        let addNotebook = UIBarButtonItem(image: #imageLiteral(resourceName: "notebook-plus"), style: .plain, target: self, action: #selector(handleCreateNotebook))
        addNotebook.tintColor = .black
        let addNote = UIBarButtonItem(image:#imageLiteral(resourceName: "file-plus"), style: .plain, target: self, action: #selector(handleCreateNote))
        addNote.tintColor = .black
        navigationItem.rightBarButtonItems = [addNotebook, addNote]
    }
    
    // MARK: - Handlers
    @objc private func handleCreateNotebook() {
        
        let alert = UIAlertController(title: "Create notebook", message: "Name for notebook", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder =  "Enter name for notebook"
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            guard let textField = alert?.textFields, let nameNotebook = textField.first?.text else { return }
            
            let result = CoreDataManager.shared.createNotebook(name: nameNotebook, notes: nil)
            result.0?.name = nameNotebook
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func handleCreateNote() {
        
        let tupleNote = CoreDataManager.shared.createNote(title: "Titulo", notebook: nil)
        let noteController = NoteViewController()
        noteController.note = tupleNote.0
        noteController.delegate = self
        
        noteController.navigationItem.leftItemsSupplementBackButton = true
        noteController.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        
        if (splitViewController?.isCollapsed)! {
            splitViewController?.showDetailViewController(noteController, sender: nil)
        } else {
            splitViewController?.showDetailViewController(UINavigationController(rootViewController: noteController), sender: nil)
        }
    }
    
    // MARK: - Functions
    func getNote (indexPath: IndexPath) -> Note {
        let notebook = getNotebook(indexPath: indexPath)
        let notes = notebook.notes?.sortedArray(using: [notesSortDescriptor]) as! [Note]
        let note = notes[indexPath.row]
        return note
    }
    
    func getNotebook (indexPath: IndexPath) -> Notebook {
        let notebook = fetchedResultsController.object(at: IndexPath(row: indexPath.section, section: 0))
        return notebook
    }
}

// MARK: - NSFetchedResultsControllerDelegate
extension NotebooksController: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.reloadData()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, sectionIndexTitleForSectionName sectionName: String) -> String? {
        return sectionName
    }
}

// MARK: - NoteViewControllerDelegate
extension NotebooksController: NoteViewControllerDelegate {
    func didSaveNote() {
        tableView.reloadData()
    }
}
