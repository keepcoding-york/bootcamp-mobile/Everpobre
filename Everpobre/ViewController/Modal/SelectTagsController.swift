//
//  SelectTagsController.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 11/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol SelectTagsControllerDelegate: class {
    func didSelectTags()
}

class SelectTagsController: UITableViewController {
    
    let CELL_ID = "CELL_ID_SELECTTAGSCONTROLLER"
    weak var delegate: SelectTagsControllerDelegate?
    let note: Note
    
    
    // MARK: - NSFetchedResultsController
    lazy var fetchedResultsController: NSFetchedResultsController<Tag> = {
        
        let context = CoreDataManager.shared.persistentContainer.viewContext
        
        let request: NSFetchRequest<Tag> = Tag.fetchRequest()
        request.sortDescriptors = [
            NSSortDescriptor(key: Tag.key.name.rawValue, ascending: true)
        ]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch let err {
            print(err)
        }
        
        return fetchedResultsController
    }()
    
    init(note: Note) {
        self.note = note
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    // MARK: - TableView DataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController.sections else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tagCell = tableView.dequeueReusableCell(withIdentifier: CELL_ID, for: indexPath) as! TagCell
        
        let tag = fetchedResultsController.object(at: indexPath)
        tagCell.textLabel?.text = tag.name
        
        if let noteContainsThisTag = note.tags?.contains(tag) {
            noteContainsThisTag ? tagCell.checkOn() : tagCell.checkOff()
        }
        
        return tagCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tag = fetchedResultsController.object(at: indexPath)
        
        guard let noteContainsThisTag = note.tags?.contains(tag) else { return }
        
        if noteContainsThisTag {
            note.removeFromTags(tag)
            tag.removeFromNote(note)
        } else {
            note.addToTags(tag)
            tag.addToNote(note)
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
        delegate?.didSelectTags()
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (_, indexPath) in
            let tag = self.fetchedResultsController.object(at: indexPath)
            
            let context = CoreDataManager.shared.persistentContainer.viewContext
            context.delete(tag)
            
            do {
                try context.save()
            } catch let saveErr {
                print("Failed to delete tag:", saveErr)
            }
        }
        deleteAction.backgroundColor = .red
        
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (_, indexPath) in
            let tag = self.fetchedResultsController.object(at: indexPath)
            
            let alert = UIAlertController(title: "Tag edit", message: nil, preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.placeholder =  "Enter name for tag"
                textField.text = tag.name
            }
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                guard let tagName = alert?.textFields![0].text else { return }
                let context = CoreDataManager.shared.persistentContainer.viewContext
                
                tag.name = tagName
                
                do {
                    try context.save()
                } catch let saveErr {
                    print("Failed to delete tag:", saveErr)
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        editAction.backgroundColor = .gray
        
        return [deleteAction, editAction]
    }
    
    // MARK: - Setup UI
    func setupUI() {
        navigationItem.title = "Choose a tag"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add tag", style: .plain, target: self, action: #selector(handleAddTag))
        
        tableView.separatorStyle = .none
        tableView.register(TagCell.self, forCellReuseIdentifier: CELL_ID)

    }
    
    // MARK: - Handlers
    @objc func handleAddTag(){
        let alert = UIAlertController(title: "Create tag", message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder =  "Enter name for tag"
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            guard let nameTag = alert?.textFields![0].text else { return }
            if !nameTag.isEmpty {
                let tupleTag = CoreDataManager.shared.createTag(name: nameTag, note: nil)
                
                if let tag = tupleTag.0 {
                    self.note.addToTags(tag)
                    tag.addToNote(self.note)
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - NSFetchedResultsControllerDelegate
extension SelectTagsController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
            tableView.reloadData()
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, sectionIndexTitleForSectionName sectionName: String) -> String? {
        return sectionName
    }
}
