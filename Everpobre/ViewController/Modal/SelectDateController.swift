//
//  SelectDateController.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 10/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import Foundation
import UIKit

protocol SelectDateControllerDelegate: class {
    func didSelectDate(date: Date)
}

class SelectDateController: UIViewController {
    
    weak var delegate: SelectDateControllerDelegate?
    var dateInitial: Date
    
    let datePickerView: UIDatePicker = {
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.timeZone = NSTimeZone.local
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = UIColor.white
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        
        return datePicker
    }()
    
    let acceptButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Accept", for: .normal)
        button.setTitleColor(.darkGray, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 24, weight: .black)
        button.addTarget(self, action: #selector(handleAccept), for: .touchUpInside)
        
        return button
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel", for: .normal)
        button.setTitleColor(.gray, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 24, weight: .black)
        button.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        
        return button
    }()
    
    //MARK: - Init
    init(date: Date = Date()) {
        self.dateInitial =  date
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        datePickerView.setDate(dateInitial, animated: true)
    }
    
    //MARK: - Setup
    func setupUI() {
        view.backgroundColor = .white
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Choose date"
    }
    
    func setupLayout() {
        acceptButton.heightAnchor.constraint(equalToConstant: 100).isActive = true
        cancelButton.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        let toolBarStackView = UIStackView(arrangedSubviews: [acceptButton, cancelButton])
        toolBarStackView.spacing = 10
        toolBarStackView.translatesAutoresizingMaskIntoConstraints = false
        toolBarStackView.distribution = .fillEqually
        
        let noteStackView = UIStackView(arrangedSubviews: [datePickerView, toolBarStackView])
        noteStackView.translatesAutoresizingMaskIntoConstraints = false
        noteStackView.axis = .vertical
        noteStackView.spacing = 10
        noteStackView.distribution = .fill
        
        view.addSubview(noteStackView)
        
        NSLayoutConstraint.activate([
            noteStackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            noteStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            noteStackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            noteStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0)
            ])
    }
    
    //MARK: - Handlers
    @objc func handleAccept() {
        delegate?.didSelectDate(date: datePickerView.date)
        navigationController?.popViewController(animated: true)
    }
    
    @objc func handleCancel() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"

        let selectedDate: String = dateFormatter.string(from: sender.date)
        print(selectedDate)
    }
}
