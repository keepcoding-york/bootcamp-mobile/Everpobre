//
//  TagCell.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 11/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

class TagCell: UITableViewCell {
    
    let checkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "tag-check"), for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageView?.contentMode = .scaleAspectFit
        button.tintColor = .lightGray
        
        return button
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        textLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        selectionStyle = .none
    }
    
    func setupLayout() {
        accessoryView = checkButton
    }
    
    func checkOn() {
        accessoryView?.tintColor = .black
    }
    func checkOff() {
        accessoryView?.tintColor = .lightGray
    }
}
