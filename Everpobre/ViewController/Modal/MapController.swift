//
//  mapController.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 12/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Contacts

protocol MapControllerDelegate: class {
    func didSaveLocation()
}

class MapController: UIViewController {
    
    weak var delegate: MapControllerDelegate?
    
    var locationCoordinate: LocationCoordinate? {
        didSet {
            guard let locationCoordinate = locationCoordinate else { return }
            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude), span: MKCoordinateSpan(latitudeDelta: distanceSpan, longitudeDelta: distanceSpan))
            mapView.setRegion(region, animated: true)
            setPin(location: locationCoordinate)
            let location = CLLocation(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude)
            setAddressFieldWithLocation(location: location)
        }
    }
    var pin = MKPointAnnotation()
    var locationManager = CLLocationManager()
    let distanceSpan: Double = 0.01
    
    let mapView: MKMapView = {
       let map = MKMapView()
        map.translatesAutoresizingMaskIntoConstraints = false
        
        return map
    }()
    let addressField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = UIColor.init(white: 1, alpha: 0.7)
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        return textField
    }()
    
    // MARK: - UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addressField.delegate = self
        mapView.delegate = self
        locationManager.delegate = self
        
        setupLayout()
        setupUI()
        setupLocation()
    }
    
    // MARK: - Setup
    func setupLocation() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        
        locationManager.desiredAccuracy =  kCLLocationAccuracyHundredMeters
    }
    
    func setupUI() {
        mapView.showsUserLocation = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save location", style: .plain, target: self, action: #selector(handleSave))
    }
    
    func setupLayout() {
        view.addSubview(mapView)
        mapView.fillSuperview()
        view.addSubview(addressField)
        addressField.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20), size: CGSize(width: 0, height: 50))
    }
    
    // MARK: - Functions
    func getLocation() {
         locationManager.startUpdatingLocation()
    }
    
    func setPin(location: LocationCoordinate){
        pin.coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
        mapView.addAnnotation(pin)
    }
    
    func setRegionWithAddress (address: String) {
        let geocoder = CLGeocoder()

        geocoder.geocodeAddressString(address) { (placemarks, error) in
            if error == nil {
                guard let placemark = placemarks?[0], let location = placemark.location else { return }
                DispatchQueue.main.async {
                    let region = MKCoordinateRegion(center: location.coordinate, span: MKCoordinateSpan.init(latitudeDelta: self.distanceSpan, longitudeDelta: self.distanceSpan))
                    self.mapView.setRegion(region, animated: true)
                }
            }
        }
    }
    
    func setAddressFieldWithLocation(location: CLLocation) {
        let geoCoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(location) { (placeMarkArray, error) in
            if let places = placeMarkArray, let place = places.first {
                DispatchQueue.main.async {
                    if let postalAdd = place.postalAddress  {
                        self.addressField.text =  "\(postalAdd.street),  \(postalAdd.city)"
                    }
                }
            }
        }
    }
    
    // MARK: - Handlers
    @objc func handleSave() {
        let centerCoord = mapView.centerCoordinate
        let context = CoreDataManager.shared.persistentContainer.viewContext
        
        locationCoordinate?.latitude = centerCoord.latitude
        locationCoordinate?.longitude = centerCoord.longitude
        
        try? context.save()
        delegate?.didSaveLocation()
        setPin(location: locationCoordinate!)
    }
}

// MARK: - MapView Delegates
extension MapController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        let centerCoord = mapView.centerCoordinate
        let location = CLLocation(latitude: centerCoord.latitude, longitude: centerCoord.longitude)
        setAddressFieldWithLocation(location: location)
    }
}


// MARK: - CLLocationManagerDelegate
extension MapController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.last else { return  }
        
        let locationCoordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: locationCoordinate, span: MKCoordinateSpan(latitudeDelta: distanceSpan, longitudeDelta: distanceSpan))
        
        mapView.setRegion(region, animated: true)
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}

// MARK: - UITextFieldDelegate
extension MapController: UITextFieldDelegate {

        func textFieldDidBeginEditing(_ textField: UITextField) {
            mapView.isScrollEnabled = false
        }
    
        func textFieldDidEndEditing(_ textField: UITextField) {
            guard let address = textField.text else { return }
            if !address.isEmpty {
                setRegionWithAddress(address: address)
            }
            mapView.isScrollEnabled = true
        }
    
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
}
