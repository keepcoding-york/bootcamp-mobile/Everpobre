//
//  ModalToSelectNotebook.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 08/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit
import CoreData

protocol SelectNotebookControllerDelegate: class {
    func didSelectNotebook(notebook: Notebook)
}

class SelectNotebookController: UITableViewController {
    
    private let CELL_ID = "CELL_ID_SELECTNOTEBOOKCONTROLLER"
    var notebooksToHide: [String]?
    weak var delegate: SelectNotebookControllerDelegate?
    
    
    // MARK: - NSFetchedResultsController
    lazy var fetchedResultsController: NSFetchedResultsController<Notebook> = {
        
        let context = CoreDataManager.shared.persistentContainer.viewContext
        let request: NSFetchRequest<Notebook> = Notebook.fetchRequest()

        if let notebooks = notebooksToHide {
            var predicates: [NSPredicate] = []
            for notebook in notebooks {
                let predicate = NSPredicate(format: "\(Notebook.key.name.rawValue)  != %@", notebook)
                predicates.append(predicate)
            }
            request.predicate = NSCompoundPredicate(orPredicateWithSubpredicates: predicates)
        }
        
        request.sortDescriptors = [
            NSSortDescriptor(key: Notebook.key.name.rawValue, ascending: true)
        ]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch let err {
            print(err)
        }
        
        return fetchedResultsController
    }()
    
    // MARK: - Init
    override init(style: UITableViewStyle) {
        super.init(style: style)
    }
    
    convenience init(notebooksToHide: [String]?) {
        self.init(style: .plain)
        guard let notebooks = notebooksToHide else { return }
        self.notebooksToHide = notebooks
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    // MARK: - TableView DataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController.sections else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) else {
                return UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: CELL_ID)
            }
            return cell
        }()
        
        let notebook = fetchedResultsController.object(at: indexPath)
        
        cell.textLabel?.text = notebook.name
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notebook = fetchedResultsController.object(at: indexPath)
        print("Notebook selected: ", notebook.name ?? "")
        delegate?.didSelectNotebook(notebook: notebook)
        navigationController?.popViewController(animated: true)
    }


    // MARK: - Setup UI
    func setupUI() {
        navigationItem.title = "Choose a notebook"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        tableView.separatorStyle = .none
    }
}

// MARK: - NSFetchedResultsControllerDelegate
extension SelectNotebookController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
            tableView.reloadData()
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, sectionIndexTitleForSectionName sectionName: String) -> String? {
        return sectionName
    }
}
