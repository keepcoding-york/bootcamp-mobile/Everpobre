//
//  RemoveNotebookController.swift
//  Everpobre
//
//  Created by Jorge Beltran Nuñez on 17/04/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

class RemoveNotebookController: UIViewController {
    var notebook: Notebook?
    
    let removeAndChooseNotebookForSaveNotesButton: UIButton = {
       let button = UIButton()
        button.setTitle("Choose notebook for save notes", for: .normal)
        button.setTitleColor(.darkGray, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        button.backgroundColor = UIColor(white: 0.95, alpha: 1)
        button.contentEdgeInsets = UIEdgeInsets(top: 15, left: 8, bottom: 15, right: 8)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleChooseNotebookAndRemove), for: .touchUpInside)
        
        return button
    }()
    
    let removeNotebookButton: UIButton = {
        let button = UIButton()
        button.setTitle("Remove notes", for: .normal)
        button.setTitleColor(.darkGray, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        button.backgroundColor = UIColor(white: 0.95, alpha: 1)
        button.contentEdgeInsets = UIEdgeInsets(top: 15, left: 8, bottom: 15, right: 8)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleRemoveNotebook), for: .touchUpInside)
        
        return button
    }()
    
    //MARK: UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupLayout()
    }
    
    //MARK: Setup
    func setupUI() {
        view.backgroundColor = .white
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleBack))
    }
    
    func setupLayout() {
        
        view.addSubview(removeNotebookButton)
        removeNotebookButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        removeNotebookButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        removeNotebookButton.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        guard let notes = notebook?.notes else { return }
        if notes.count > 0 {
            view.addSubview(removeAndChooseNotebookForSaveNotesButton)
            removeAndChooseNotebookForSaveNotesButton.topAnchor.constraint(equalTo: removeNotebookButton.bottomAnchor, constant: 20).isActive = true
            removeAndChooseNotebookForSaveNotesButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
            removeAndChooseNotebookForSaveNotesButton.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        }
    }
    
    //MARK: Handlers
    @objc func handleBack() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func handleRemoveNotebook() {
        removeNotebook()
        dismiss(animated: true, completion: nil)
    }
    
    @objc func handleChooseNotebookAndRemove() {
        let selectNotebookController = SelectNotebookController(notebooksToHide: [(notebook?.name)!])
        selectNotebookController.delegate = self
        navigationController?.pushViewController(selectNotebookController, animated: true)
    }
    
    //MARK: Functions
    func removeNotebook() {
        let context = CoreDataManager.shared.persistentContainer.viewContext
        context.delete(notebook!)
        
        do {
            try context.save()
        } catch let err {
            print("Failed to create notebook:", err)
        }
    }
}

//MARK: SelectNotebookControllerDelegate
extension RemoveNotebookController: SelectNotebookControllerDelegate {
    func didSelectNotebook(notebook: Notebook) {
        guard let notes = self.notebook?.notes?.allObjects as? [Note]  else { return }
        
        notes.forEach { note in
            note.notebook = notebook
        }
        
        removeNotebook()
        dismiss(animated: true, completion: nil)
    }
}
